# Muffin Dev for Unity - Unity Directory Cleaner

This package is a utility that cleans a Unity project directory, by removing all the automatically-generated files (like Visual Studio solutions, `/Library` directory, logs, etc.).

This is useful if you want to upload your project or reset the cache.

***IMPORTANT*: Since there's no Unity file or else that can specify that a directory contains a Unity project, this utility is based on the directory structure. A directory is considered as a Unity project if it contains an `/Assets` and a `/ProjectSettings` directory.**

## Installation

Install it globally using *npm*:

```bash
npm i -g @muffin-dev/unity-directory-cleaner
```

## Usage

Once installed, you can clean a repository by running command:

```bash
unity-directory-cleaner /path/to/your/unity/project
```

You can also uses the command alias as a shortcut:

```bash
unity-dc /path/to/your/unity/project
```

### Clean recursively

If you have a directory that contains several Unity projects, and you want to clean all of them, you can use the `--recursive` option (or its alias `-r`):

```bash
unity-directory-cleaner /path/to/your/unity/project --recursive
unity-directory-cleaner /path/to/your/unity/project -r
```

This will browse all the Unity project directories, and clean them.

## What will this utility do?

1. It removes the Unity auto-generated directories:

- `/Library`
- `/Logs`
- `/obj`
- `/Temp`

2. It removes the C# solution, VS Code and "regular" Visual Studio files:

- `/.vs`
- `/.vscode`
- `*.csproj`
- `*.sln`
- `*.suo`

That's it! If you need this tool to clean any other file or directory not referenced here, **feel free to contact me, and I'll add them to the cleaning process!**

## Future improvements

Use external JSON file to define what are the files and directories to clean. A default one will be included in the app, but you will be able to create your own one and use it by specifying its path to the CLI command.

Defines minimum and maximum Unity versions for files and directories to clean.

**Feel free to contact me if you need these improvements! ;)**