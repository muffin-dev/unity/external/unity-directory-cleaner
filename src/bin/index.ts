#!/usr/bin/env node
import clean from '../lib';

// process.argv[2] should be the path to the Unity project directory to clean
const path = process.argv[2];

// process.argv[3] should be the "recursive" option
const recursive = process.argv[3] && (process.argv[3] === '--recursive' || process.argv[3] === '-r');

clean(path, recursive);
